#pragma once

#ifdef GSTATS_HANDLE_STATS
#   ifndef __AND
#      define __AND ,
#   endif
#   define GSTATS_HANDLE_STATS_USER(gstats_handle_stat) \
        gstats_handle_stat(LONG_LONG, commit_tx_reader, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
          __AND gstats_output_item(PRINT_RAW, SUM, BY_THREAD) \
        }) \
        gstats_handle_stat(LONG_LONG, commit_tx_writer, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
          __AND gstats_output_item(PRINT_RAW, SUM, BY_THREAD) \
        }) \
        gstats_handle_stat(LONG_LONG, abort_tx, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
          __AND gstats_output_item(PRINT_RAW, SUM, BY_THREAD) \
        }) \
        gstats_handle_stat(LONG_LONG, ht_capacity, 64, { \
                gstats_output_item(PRINT_RAW, MAX, BY_THREAD) \
        }) \
        gstats_handle_stat(LONG_LONG, tx_rdset_size, 1000, { \
                gstats_output_item(PRINT_HISTOGRAM_LIN, NONE, FULL_DATA) \
          __AND gstats_output_item(PRINT_TO_FILE, NONE, FULL_DATA) \
        }) \
        gstats_handle_stat(LONG_LONG, tx_wrset_size, 1000, { \
                gstats_output_item(PRINT_HISTOGRAM_LIN, NONE, FULL_DATA) \
          __AND gstats_output_item(PRINT_TO_FILE, NONE, FULL_DATA) \
        }) \
        gstats_handle_stat(LONG_LONG, fasthtm_commit, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, fasthtm_abort, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, htmpostfix_commit, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, htmpostfix_abort, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, htmprefix_commit, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, htmprefix_abort, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, htmprefix_abort_explicit, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, htmprefix_abort_conflict, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, htmprefix_abort_capacity, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, htmprefix_abort_nesting, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, htmprefix_abort_illegal, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, htmprefix_abort_zero, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, stmprefix_abort, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, stmprefix_commit, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, slow_commit, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, abtree_insert_repeat, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, abtree_erase_repeat, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, abtree_search_repeat, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, abtree_search_complete, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, abtree_fix_weight_viol_attempt, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, abtree_fix_degree_viol_attempt, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, abtree_rebalancing_kcas_attempt, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, abtree_update_kcas_attempt, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, abtree_internal_complex_search_while_iters, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, abtree_internal_basic_search_while_iters, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \

    // define a variable for each stat above
    GSTATS_HANDLE_STATS_USER(__DECLARE_EXTERN_STAT_ID);
#endif
