#!/usr/bin/python3
from _basic_functions import *

def define_experiment(exp_dict, args):
    set_dir_compile  ( exp_dict, os.getcwd() + '/../' )
    set_dir_tools    ( exp_dict, os.getcwd() + '/../setbench/tools' )
    set_dir_run      ( exp_dict, os.getcwd() + '/bin' )
    set_dir_data     ( exp_dict, os.getcwd() + '/data' )

    add_run_param    ( exp_dict, 'INS_DEL_FRAC'    , ['50.0 50.0'] )
    add_run_param    ( exp_dict, 'MAXKEY'          , [2000000] )
    add_run_param    ( exp_dict, 'algorithm'       , [
              'brown_int_bst_tm_auto.debra.hytm1'
            , 'brown_int_bst_tm_auto.debra.hytm2'
            , 'brown_int_bst_tm_auto.debra.tl2'
            , 'brown_int_bst_tm_auto.debra.rhnorec_post'
            , 'brown_int_bst_tm_auto.debra.rhnorec_post_pre'
            , 'brown_sigouin_int_avl_tm_auto.debra.hytm1'
            , 'brown_sigouin_int_avl_tm_auto.debra.hytm2'
            , 'brown_sigouin_int_avl_tm_auto.debra.tl2'
            , 'brown_sigouin_int_avl_tm_auto.debra.rhnorec_post'
            , 'brown_sigouin_int_avl_tm_auto.debra.rhnorec_post_pre'
            , 'brown_abtree_tm_auto.debra.hytm1'
            , 'brown_abtree_tm_auto.debra.hytm2'
            , 'brown_abtree_tm_auto.debra.tl2'
            , 'brown_abtree_tm_auto.debra.rhnorec_post'
            , 'brown_abtree_tm_auto.debra.rhnorec_post_pre'
            , 'sigouin_int_bst_kcas.debra'
            , 'sigouin_int_bst_kcas_htm.debra'
            , 'sigouin_int_avl_kcas.debra'
            , 'sigouin_int_avl_kcas_htm.debra'
            , 'bronson_pext_bst_occ.debra'
            , 'natarajan_ext_bst_lf.debra'
            , 'brown_ext_abtree_lf.debra'
            , 'brown_sigouin_abtree_kcas.debra'
            , 'brown_sigouin_abtree_kcas_htm.debra'
            , 'wang_openbwtree'
    ] )
    add_run_param    ( exp_dict, 'thread_pinning'  , ['-pin ' + shell_to_str('cd ' + get_dir_tools(exp_dict) + ' ; ./get_pinning_cluster.sh', exit_on_error=True)] )
    add_run_param    ( exp_dict, 'millis'          , [10000] )
    add_run_param    ( exp_dict, '__trials'        , [1, 2] )
    add_run_param    ( exp_dict, 'TOTAL_THREADS'   , [48, 190] )

    cmd_run = 'LD_PRELOAD=../../setbench/lib/libmimalloc.so timeout 120 numactl --interleave=all time ./{algorithm} -nwork {TOTAL_THREADS} -nprefill {TOTAL_THREADS} -insdel {INS_DEL_FRAC} -k {MAXKEY} -t {millis} {thread_pinning} -rq 0 -rqsize 1 -nrq 0'
    if args.testing:
        add_run_param( exp_dict, '__trials'        , [1] )
        add_run_param( exp_dict, 'TOTAL_THREADS'   , shell_to_listi('cd ' + get_dir_tools(exp_dict) + ' ; ./get_thread_counts_max.sh', exit_on_error=True) )
        add_run_param( exp_dict, 'millis'          , [500] )
        cmd_run = cmd_run.replace('-nprefill {TOTAL_THREADS}', '-nprefill 0')

    set_cmd_run      ( exp_dict, cmd_run )
    set_cmd_compile  ( exp_dict, 'make bin_dir={__dir_run} -j' )

    add_data_field   ( exp_dict, 'alg'               , coltype='TEXT', extractor=get_alg, validator=is_nonempty )
    add_data_field   ( exp_dict, 'total_throughput'  , coltype='INTEGER', validator=is_positive )
    add_data_field   ( exp_dict, 'PAPI_L3_TCM'       , coltype='REAL' )
    add_data_field   ( exp_dict, 'PAPI_L2_TCM'       , coltype='REAL' )
    add_data_field   ( exp_dict, 'PAPI_TOT_CYC'      , coltype='REAL' )
    add_data_field   ( exp_dict, 'PAPI_TOT_INS'      , coltype='REAL' )
    add_data_field   ( exp_dict, 'maxresident_mb'    , coltype='REAL', validator=is_positive, extractor=get_maxres ) ## note the custom extractor
    add_data_field   ( exp_dict, 'validate_result'   , coltype='TEXT', validator=is_equal('success') )
    add_data_field   ( exp_dict, 'MILLIS_TO_RUN'     , coltype='TEXT', validator=is_positive )
    add_data_field   ( exp_dict, 'RECLAIM'           , coltype='TEXT' )

    ## create legend to put below each table of plots
    add_plot_set(exp_dict, name='legend.png', series='alg', x_axis='TOTAL_THREADS', y_axis='total_throughput'
            , plot_type=plot_line_regions_legend)

    ## create tables of plots
    for field in ['total_throughput', 'PAPI_L2_TCM', 'PAPI_L3_TCM', 'PAPI_TOT_CYC', 'PAPI_TOT_INS', 'maxresident_mb']:
        add_plot_set(
                exp_dict
              , name=field+'-u{INS_DEL_FRAC}-k{MAXKEY}.png'
              , title=field
              , varying_cols_list=['INS_DEL_FRAC', 'MAXKEY']
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis=field
              , plot_type=plot_line_regions
        )
        add_page_set(
                exp_dict
              , image_files=field+'-u{INS_DEL_FRAC}-k{MAXKEY}.png'
              , legend_file='legend.png'
        )



def get_maxres(exp_dict, file_name, field_name):
    maxres_kb_str = shell_to_str('grep "maxres" {} | cut -d" " -f6 | cut -d"m" -f1'.format(file_name))
    if maxres_kb_str:
        return float(maxres_kb_str) / 1000
    return 0

alg_rename = dict({
              'brown_int_bst_tm_auto.debra.tl2': 'int-bst-tl2'
            , 'brown_int_bst_tm_auto.debra.hytm1': 'int-bst-tle'
            , 'brown_int_bst_tm_auto.debra.rhnorec_post': 'int-bst-rh'
            , 'brown_int_bst_tm_auto.debra.rhnorec_post_pre': 'int-bst-rh+'
            , 'brown_sigouin_int_avl_tm_auto.debra.tl2': 'int-avl-tl2'
            , 'brown_sigouin_int_avl_tm_auto.debra.hytm1': 'int-avl-tle'
            , 'brown_sigouin_int_avl_tm_auto.debra.rhnorec_post': 'int-avl-rh'
            , 'brown_sigouin_int_avl_tm_auto.debra.rhnorec_post_pre': 'int-avl-rh+'
            , 'sigouin_int_bst_kcas.debra': 'int-bst-kcas'
            , 'sigouin_int_bst_kcas_htm.debra': 'int-bst-kcas+'
            , 'sigouin_int_avl_kcas.debra': 'int-avl-kcas'
            , 'sigouin_int_avl_kcas_htm.debra': 'int-avl-kcas+'
            , 'bronson_pext_bst_occ.debra': 'pext-avl-occ'
            , 'natarajan_ext_bst_lf.debra': 'ext-bst-lf'
            , 'brown_ext_abtree_lf.debra': 'abtree-lf'
            , 'brown_sigouin_abtree_kcas.debra': 'abtree-kcas'
            , 'brown_sigouin_abtree_kcas_htm.debra': 'abtree-kcas+'
            , 'wang_openbwtree': 'open-bwtree'
})
def get_alg(exp_dict, file_name, field_name):
    result = grep_line(exp_dict, file_name, 'algorithm')
    return alg_rename[result]

import pandas
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
from run_experiment import get_seaborn_series_styles
def plot_line_regions(filename, column_filters, data, series_name, x_name, y_name, exp_dict, do_save=True, legend=False, do_close=True):
    plt.ioff() # stop plots from being shown in jupyter
    # print('series_name={}'.format(series_name))

    plot_kwargs = get_seaborn_series_styles(series_name, plot_func=sns.lineplot, exp_dict=exp_dict)
    plot_kwargs['style'] = series_name
    plot_kwargs['markersize'] = 12
    plt.style.use('dark_background')
    if not legend: plot_kwargs['legend'] = False

    fig, ax = plt.subplots()
    sns.lineplot(ax=ax, data=data, x=x_name, y=y_name, hue=series_name, ci=100, **plot_kwargs)
    plt.tight_layout()

    if do_save: mpl.pyplot.savefig(filename)
    # print(data) ; print('## SAVED FIGURE {}'.format(filename))
    if do_close: plt.close()

def plot_line_regions_legend(filename, column_filters, data, series_name, x_name, y_name, exp_dict):
    plot_line_regions(filename, column_filters, data, series_name, x_name, y_name, do_save=False, legend=True, exp_dict=exp_dict, do_close=False)
    axi = plt.gca()
    handles, labels = axi.get_legend_handles_labels()

    fig_legend, axi = plt.subplots()
    fig_legend.legend(handles[1:], labels[1:], loc='center', frameon=False, ncol=4)

    axi.xaxis.set_visible(False)
    axi.yaxis.set_visible(False)
    axi.axes.set_visible(False)

    plt.tight_layout()
    fig_legend.savefig(filename, bbox_inches='tight')
    plt.close()
